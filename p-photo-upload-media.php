<?php include_once("initial.php"); ?>
<?php
define("TEMPLATE_TITLE", $messages["GOOGLE_PHOTO_UPLOAD_MEDIA"]);
$file = "/tmp/albums.ini";
if (!file_exists($file)) {
	$handle = fopen($file, "w");
	fwrite($handle, "[album]");
	include_once("HttpRequest.php");
	$query["pageSize"] = 50;
	while (true) {
		$httpRequest = new HttpRequest("https://photoslibrary.googleapis.com/v1/albums?" . http_build_query($query, null, "&"));
		$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
		$httpRequest->sendRequest("GET");
		$json = json_decode($httpRequest->getResponseData(), true);
		foreach ($json["albums"] as $v) {
			fwrite($handle, "\n" . $v["id"] . "=\"" . $v["title"] . "\"");
		}
		if (isset($json["nextPageToken"])) {
			$query["pageToken"] = $json["nextPageToken"];
		} else {
			break;
		}
	}
	fflush($handle);
	fclose($handle);
}
$albums = parse_ini_file($file, true);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($_SESSION["token_expires_in"]) && isset($_SESSION["token_creation_time"])) { ?>
<?php if (($remain = ($_SESSION["token_expires_in"] + $_SESSION["token_creation_time"] - time())) > 0) { ?>
			<form action="a-photo-upload-media.php" method="post" enctype="multipart/form-data">
				<div>
					<label><?php echo $messages["GOOGLE_PHOTO_SELECT_ALBUM"]; ?></label>
					<select name="albumId">
						<option value="" selected="selected"><?php echo sprintf("========== %s ==========", $messages["GOOGLE_PHOTO_NO_ASSIGN_ALBUM"]); ?></option>
<?php foreach ($albums["album"] as $k => $v) { ?>
						<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
<?php } ?>
					</select>
					<a href="a-photo-renew-albums.php"><?php echo $messages["GOOGLE_PHOTO_RELOAD_ALBUM"]; ?></a>
				</div>
				<div>
					<input type="file" name="files[]" multiple="multiple"/>
				</div>
				<div>
					<input type="submit" name="submit" value="<?php echo TEMPLATE_TITLE; ?>"/>
				</div>
			</form>
<?php } else { ?>
			<div><?php echo sprintf($messages["GOOGLE_OAUTH2_TOKEN_EXPIRED"], -$remain); ?></div>
<?php } ?>
<?php } else { ?>
			<div><?php echo $messages["ERROR_NO_TOKEN"]; ?></div>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>