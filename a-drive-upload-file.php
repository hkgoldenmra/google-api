<?php

include_once("initial.php");
include_once("HttpRequest.php");
define("TEMPLATE_TITLE", $messages["GOOGLE_DRIVE_UPLOAD_FILE"]);

$files = array(
	"success" => array(),
	"failure" => array(),
);
foreach (array_keys($_FILES["files"]["name"]) as $k) {
	if ($_FILES["files"]["error"][$k] == 0) {
		$data = array();
		$data["name"] = $_FILES["files"]["name"][$k];
		if (isset($_POST["fileId"]) && strlen($_POST["fileId"]) > 0) {
			$data["parents"] = $_POST["fileId"];
		}
		$data = json_encode($data);
		$boundary = base64_encode(time());
		$httpRequest = new HttpRequest("https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart");
		$httpRequest->addHeader("Content-Type", "multipart/related; boundary=" . $boundary);
		$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
		$httpRequest->addData("--" . $boundary);
		$httpRequest->addData("\nContent-Type: application/json");
		$httpRequest->addData("\n");
		$httpRequest->addData("\n");
		$httpRequest->addData($data);
		$httpRequest->addData("\n--" . $boundary);
		$httpRequest->addData("\nContent-Type: " . $_FILES["files"]["type"][$k]);
		$httpRequest->addData("\n");
		$httpRequest->addData("\n");
		$httpRequest->addFile($_FILES["files"]["tmp_name"][$k]);
		$httpRequest->addData("\n--" . $boundary . "--");
		$httpRequest->sendRequest("POST");

		$json = $httpRequest->getResponseData();
		if (isset($json["error"])) {
			array_push($files["failure"], $_FILES["files"]["name"][$k]);
		} else {
			array_push($files["success"], $_FILES["files"]["name"][$k]);
		}
	} else {
		array_push($files["failure"], $_FILES["files"]["name"][$k]);
	}
}
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (count($files["success"]) > 0) { ?>
			<div><?php echo $messages["BASIC_SUCCESS"]; ?></div>
<?php foreach ($files["success"] as $v) { ?>
			<div><?php echo $v; ?></div>
<?php } ?>
<?php } ?>
<?php if (count($files["failure"]) > 0) { ?>
			<div><?php echo $messages["BASIC_FAILURE"]; ?></div>
<?php foreach ($files["failure"] as $v) { ?>
			<div><?php echo $v; ?></div>
<?php } ?>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>