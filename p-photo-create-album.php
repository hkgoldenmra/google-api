<?php include_once("initial.php"); ?>
<?php
define("TEMPLATE_TITLE", $messages["GOOGLE_PHOTO_CREATE_ALBUM"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($_SESSION["token_expires_in"]) && isset($_SESSION["token_creation_time"])) { ?>
<?php if (($remain = ($_SESSION["token_expires_in"] + $_SESSION["token_creation_time"] - time())) > 0) { ?>
			<form action="a-photo-create-album.php" method="post">
				<div>
					<label><?php echo $message["GOOGLE_PHOTO_ALBUM_TITLE"]; ?></label>
					<input type="text" name="title"/>
				</div>
				<div>
					<input type="submit" name="submit" value="<?php echo TEMPLATE_TITLE; ?>"/>
				</div>
			</form>
<?php } else { ?>
			<div><?php echo sprintf($messages["GOOGLE_OAUTH2_TOKEN_EXPIRED"], -$remain); ?></div>
<?php } ?>
<?php } else { ?>
			<div><?php echo $messages["ERROR_NO_TOKEN"]; ?></div>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>