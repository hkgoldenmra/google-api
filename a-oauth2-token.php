<?php

include_once("initial.php");

$data["redirect_uri"] = "urn:ietf:wg:oauth:2.0:oob";
$data["grant_type"] = "authorization_code";
$data["client_id"] = $_SESSION["client_id"];
$data["client_secret"] = $_SESSION["client_secret"] = $_POST["client_secret"];
$data["code"] = $_POST["code"];
$data = urldecode(http_build_query($data, null, "&"));

include_once("HttpRequest.php");
$httpRequest = new HttpRequest("https://oauth2.googleapis.com/token");
$httpRequest->addHeader("Content-Type", "application/x-www-form-urlencoded");
$httpRequest->addData($data);
$httpRequest->sendRequest("POST");

$json = json_decode($httpRequest->getResponseData(), true);
?>
<?php if (isset($json["error"])) { ?>
<?php
define("TEMPLATE_TITLE", $messages["GOOGLE_OAUTH2_GET_TOKEN"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
			<div><?php echo sprintf($messages["ERROR"], $json["error"]); ?></div>
			<div><?php echo sprintf($messages["ERROR_MESSAGE"], $json["error_description"]); ?></div>
		</fieldset>
<?php include_once("t-html-end.php"); ?>
<?php } else { ?>
<?php
$_SESSION["token_type"] = $json["token_type"];
$_SESSION["access_token"] = $json["access_token"];
$_SESSION["refresh_token"] = $json["refresh_token"];
$_SESSION["token_expires_in"] = $json["expires_in"] - 0;
$_SESSION["token_creation_time"] = time() - 0;
header("Location: index.php");
?>
<?php } ?>