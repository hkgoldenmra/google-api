<?php

session_start();
$availableLanguages = array("DEFAULT" => "en", "zh");
$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
if (!in_array($language, $availableLanguages)) {
	$language = $availableLanguages["DEFAULT"];
}
$messages = parse_ini_file("message/" . $language . ".ini", true);