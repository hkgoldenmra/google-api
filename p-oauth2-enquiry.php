<?php include_once("initial.php"); ?>
<?php
define("TEMPLATE_TITLE", $messages["GOOGLE_OAUTH2_ENQUIRY_TOKEN"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($_SESSION["token_expires_in"]) && isset($_SESSION["token_creation_time"])) { ?>
<?php if (($remain = ($_SESSION["token_expires_in"] + $_SESSION["token_creation_time"] - time())) > 0) { ?>
			<div><?php echo sprintf($messages["GOOGLE_OAUTH2_TOKEN_ALIVE"], $remain); ?></div>
<?php } else { ?>
			<div><?php echo sprintf($messages["GOOGLE_OAUTH2_TOKEN_EXPIRED"], -$remain); ?></div>
<?php } ?>
<?php } else { ?>
			<div><?php echo $messages["ERROR_NO_TOKEN"]; ?></div>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>