<?php include_once("initial.php"); ?>
<?php
define("TEMPLATE_TITLE", $messages["GOOGLE_OAUTH2_GET_TOKEN"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($_SESSION["client_id"])) { ?>
			<form action="a-oauth2-token.php" method="post">
				<div>
					<label><?php echo $messages["GOOGLE_API_PASSWORD"]; ?></label>
					<input type="password" name="client_secret"/>
				</div>
				<div>
					<label><?php echo $messages["GOOGLE_OAUTH2_CODE"]; ?></label>
					<input type="text" name="code"/>
				</div>
				<div>
					<input type="submit" name="submit" value="<?php echo TEMPLATE_TITLE; ?>"/>
				</div>
			</form>
<?php } else { ?>
			<div><?php echo $messages["ERROR_ACCOUNT_NOT_SET"]; ?></div>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>