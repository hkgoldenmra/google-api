<?php

include_once("initial.php");

$data["token"] = $_SESSION["access_token"];
$data = urldecode(http_build_query($data, null, "&"));

include_once("HttpRequest.php");
$httpRequest = new HttpRequest("https://oauth2.googleapis.com/revoke?" . $data);
$httpRequest->sendRequest("POST");
define("TEMPLATE_TITLE", $messages["GOOGLE_OAUTH2_REVOKE_TOKEN"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if ($httpRequest->getResponseCode() != 200) { ?>
<?php
$json = json_decode($httpRequest->getResponseData(), true);
?>
			<div><?php echo sprintf($messages["ERROR"], $json["error"]); ?></div>
			<div><?php echo sprintf($messages["ERROR_MESSAGE"], $json["error_description"]); ?></div>
<?php } else { ?>
			<div><?php echo $messages["BASIC_SUCCESS"]; ?></div>
<?php
unset($_SESSION["client_id"]);
unset($_SESSION["client_secret"]);
unset($_SESSION["token_type"]);
unset($_SESSION["access_token"]);
unset($_SESSION["refresh_token"]);
unset($_SESSION["token_expires_in"]);
unset($_SESSION["token_creation_time"]);
?>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>