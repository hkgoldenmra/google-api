<?php

include_once("initial.php");

$data["grant_type"] = "refresh_token";
$data["client_id"] = $_SESSION["client_id"];
$data["client_secret"] = $_SESSION["client_secret"];
$data["refresh_token"] = $_SESSION["refresh_token"];
$data = urldecode(http_build_query($data, null, "&"));

include_once("HttpRequest.php");
$httpRequest = new HttpRequest("https://oauth2.googleapis.com/token");
$httpRequest->addHeader("Content-Type", "application/x-www-form-urlencoded");
$httpRequest->addData($data);
$httpRequest->sendRequest("POST");

$json = json_decode($httpRequest->getResponseData(), true);
define("TEMPLATE_TITLE", $messages["GOOGLE_OAUTH2_REFRESH_TOKEN"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($json["error"])) { ?>
			<div><?php echo sprintf($messages["ERROR"], $json["error"]); ?></div>
			<div><?php echo sprintf($messages["ERROR_MESSAGE"], $json["error_description"]); ?></div>
<?php } else { ?>
			<div><?php echo $messages["BASIC_SUCCESS"]; ?></div>
<?php
$_SESSION["access_token"] = $json["access_token"];
$_SESSION["token_expires_in"] = $json["expires_in"] - 0;
$_SESSION["token_creation_time"] = time() - 0;
?>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>