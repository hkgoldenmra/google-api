<?php include_once("initial.php"); ?>
<?php
$scopes = parse_ini_file("scope.ini", true);
define("TEMPLATE_TITLE", $messages["GOOGLE_OAUTH2_GET_CODE"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
			<form action="a-oauth2-code.php" method="get" target="_blank">
				<div>
					<label><?php echo $messages["GOOGLE_API_ACCOUNT"]; ?></label>
					<input type="text" name="client_id"/>
				</div>
				<div>
					<label><?php echo $messages["GOOGLE_API_SCOPE"]; ?></label>
					<select name="scope[]" multiple="multiple" size="20">
<?php foreach ($scopes as $k0 => $v0) { ?>
						<optgroup label="<?php echo $k0; ?>">
<?php foreach ($v0 as $k1 => $v1) { ?>
							<option value="<?php echo $k1; ?>"><?php echo $v1; ?></option>
<?php } ?>
						</optgroup>
<?php } ?>
					</select>
				</div>
				<div>
					<input type="submit" name="submit" value="<?php echo TEMPLATE_TITLE; ?>"/>
				</div>
			</form>
		</fieldset>
<?php include_once("t-html-end.php"); ?>