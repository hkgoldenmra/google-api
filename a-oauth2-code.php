<?php

include_once("initial.php");
$data["redirect_uri"] = "urn:ietf:wg:oauth:2.0:oob";
$data["response_type"] = "code";
$data["client_id"] = $_SESSION["client_id"] = $_GET["client_id"];
$data["scope"] = implode("+", $_GET["scope"]);
$data = urldecode(http_build_query($data, null, "&"));
header("Location: https://accounts.google.com/o/oauth2/v2/auth?" . $data);