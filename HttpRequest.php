<?php
class HttpRequest {
	private static final function NO_REQUEST() {
		return "No request has been sent.";
	}
	private static final function NOT_AVAILABLE_METHOD() {
		return "Not available HTTP method.";
	}
	private static final function AVAILABLE_METHODS() {
		return array("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS", "PATCH");
	}
	private $requestURL;
	private $requestHeaders = array();
	private $requestData = "";
	private $responseCode = 0;
	private $responseMessage = "";
	private $responseHeaders = array();
	private $responseData = null;
	public function __construct($url) {
		$this->requestURL = parse_url($url);
		$this->requestURL["socket"] = (($this->requestURL["scheme"] == "http") ? "" : "ssl://");
		if (!isset($this->requestURL["port"])) {
			$this->requestURL["port"] = (($this->requestURL["scheme"] == "http") ? 80 : 443);
		}
		if (!isset($this->requestURL["query"])) {
			$this->requestURL["query"] = "";
		}
		if (!isset($this->requestURL["fragment"])) {
			$this->requestURL["fragment"] = "";
		}
		if (!isset($this->requestURL["user"])) {
			$this->requestURL["user"] = "";
		}
		if (!isset($this->requestURL["pass"])) {
			$this->requestURL["pass"] = "";
		}
		$this->resetHeaders();
	}
	public function resetHeaders() {
		$this->requestHeaders = array();
	}
	public function addHeader($key, $value) {
		if (isset($this->requestHeaders[$key])) {
			$this->requestHeaders[$key] .= ", " . $value;
		} else {
			$this->requestHeaders[$key] = $value;
		}
		ksort($this->requestHeaders);
	}
	public function resetData() {
		$this->requestData = "";
	}
	public function addData($data) {
		$this->requestData .= $data;
	}
	public function addFile($file) {
		if (($handle = fopen($file, "r")) !== false) {
			while (!feof($handle)) {
				$this->addData(fread($handle, 4096));
			}
			fclose($handle);
		}
	}
	public function addURL($url) {
		$httpRequest = new HttpRequest($url);
		$httpRequest->sendRequest("GET");
		$this->addData($httpRequest->getResponseData());
	}
	public function sendRequest($method = "GET") {
		$method = strtoupper($method);
		if (in_array($method, self::AVAILABLE_METHODS())) {
			$errno = 0;
			$error = null;
			if (($handle = fsockopen($this->requestURL["socket"] . $this->requestURL["host"], $this->requestURL["port"], $errno, $error, 30)) !== false) {
				$length = strlen($this->requestData);
				$this->addHeader("Host", $this->requestURL["host"] . ":" . $this->requestURL["port"]);
				$this->addHeader("Connection", "close");
				$this->addHeader("Content-Length", $length);
				$requestString = "";
				$requestString .= $method . " " . $this->requestURL["path"] . "?" . $this->requestURL["query"] . "#" . $this->requestURL["fragment"] . " HTTP/1.1\n";
				foreach ($this->requestHeaders as $k => $v) {
					$requestString .= $k . ": " . $v . "\n";
				}
				$requestString .= "\n" . $this->requestData;
				fwrite($handle, $requestString);
				$responseString = "";
				while (!feof($handle)) {
					$responseString .= fread($handle, 4096);
				}
				fclose($handle);
				$this->responseData = explode("\r\n\r\n", $responseString, 2);
				$this->responseHeaders = explode("\r\n", $this->responseData[0]);
				$this->responseMessage = explode(" ", array_shift($this->responseHeaders), 3);
				$this->responseCode = $this->responseMessage[1] - 0;
				$this->responseMessage = $this->responseMessage[2];
				foreach ($this->responseHeaders as $k => $v) {
					$v = explode(": ", $v, 2);
					$this->responseHeaders[$v[0]] = $v[1];
					unset($this->responseHeaders[$k]);
				}
				ksort($this->responseHeaders);
				$handle = $this->responseData[1];
				if (isset($this->responseHeaders["Transfer-Encoding"]) && $this->responseHeaders["Transfer-Encoding"] == "chunked") {
					$this->responseData = "";
					$handle = explode("\r\n", $handle, 2);
					while (($length = hexdec($handle[0])) > 0) {
						$this->responseData .= substr($handle[1], 0, $length);
						$handle = explode("\r\n", substr($handle[1], $length + 2), 2);
					}
				} else {
					$this->responseData = $handle;
				}
			} else {
				throw new \Exception($errno . ": " . $error);
			}
		} else {
			throw new \Exception(self::NOT_AVAILABLE_METHOD());
		}
	}
	public function getResponseCode() {
		if ($this->responseCode === 0) {
			throw new \Exception(self::NO_REQUEST());
		} else {
			return $this->responseCode;
		}
	}
	public function getResponseMessage() {
		if ($this->responseCode === 0) {
			throw new \Exception(self::NO_REQUEST());
		} else {
			return $this->responseMessage;
		}
	}
	public function getResponseHeaders() {
		if ($this->responseCode === 0) {
			throw new \Exception(self::NO_REQUEST());
		} else {
			return $this->responseHeaders;
		}
	}
	public function getResponseData() {
		if ($this->responseCode === 0) {
			throw new \Exception(self::NO_REQUEST());
		} else {
			return $this->responseData;
		}
	}
}