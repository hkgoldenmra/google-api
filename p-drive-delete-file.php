<?php include_once("initial.php"); ?>
<?php
include_once("HttpRequest.php");
$query["pageSize"] = 1000;
$query["q"] = "mimeType='application/vnd.google-apps.folder'";
$query = urldecode(http_build_query($query, null, "&"));
$httpRequest = new HttpRequest("https://www.googleapis.com/drive/v3/files?" . $query);
$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
$httpRequest->sendRequest("GET");
$json = json_decode($httpRequest->getResponseData(), true);
usort($json["files"], function($o1, $o2) {
	if ($o1["mimeType"] == "application/vnd.google-apps.folder" && $o2["mimeType"] == "application/vnd.google-apps.folder") {
		return strcmp($o1["name"], $o2["name"]);
	} else if ($o1["mimeType"] == "application/vnd.google-apps.folder") {
		return -1;
	} else if ($o2["mimeType"] == "application/vnd.google-apps.folder") {
		return 1;
	} else {
		return strcmp($o1["name"], $o2["name"]);
	}
});
define("TEMPLATE_TITLE", $messages["GOOGLE_DRIVE_DELETE_FILE"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($_SESSION["token_expires_in"]) && isset($_SESSION["token_creation_time"])) { ?>
<?php if (($remain = ($_SESSION["token_expires_in"] + $_SESSION["token_creation_time"] - time())) > 0) { ?>
			<form action="a-drive-delete-file.php" method="post">
				<div>
					<label><?php echo $messages["GOOGLE_PHOTO_SELECT_ALBUM"]; ?></label>
					<select name="fileId">
<?php foreach ($json["files"] as $v) { ?>
						<option value="<?php echo $v["id"]; ?>"><?php echo $v["name"]; ?></option>
<?php } ?>
					</select>
				</div>
				<div>
					<input type="submit" name="submit" value="<?php echo TEMPLATE_TITLE; ?>"/>
				</div>
			</form>
<?php } else { ?>
			<div><?php echo sprintf($messages["GOOGLE_OAUTH2_TOKEN_EXPIRED"], -$remain); ?></div>
<?php } ?>
<?php } else { ?>
			<div><?php echo $messages["ERROR_NO_TOKEN"]; ?></div>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>