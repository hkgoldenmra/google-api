<?php include_once("initial.php"); ?>
<?php
define("TEMPLATE_TITLE", $messages["GOOGLE_DRIVE_UPLOAD_FILE"]);
$file = "/tmp/folders.ini";
if (!file_exists($file)) {
	$handle = fopen($file, "w");
	fwrite($handle, "[folder]");
	include_once("HttpRequest.php");
	$query["pageSize"] = 1000;
	$query["q"] = "mimeType='application/vnd.google-apps.folder'";
//	while (true) {
		$httpRequest = new HttpRequest("https://www.googleapis.com/drive/v3/files?" . http_build_query($query, null, "&"));
		$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
		$httpRequest->sendRequest("GET");
		$json = json_decode($httpRequest->getResponseData(), true);
		usort($json["files"], function($o1, $o2) {
			if ($o1["mimeType"] == "application/vnd.google-apps.folder" && $o2["mimeType"] == "application/vnd.google-apps.folder") {
				return strcmp($o1["name"], $o2["name"]);
			} else if ($o1["mimeType"] == "application/vnd.google-apps.folder") {
				return -1;
			} else if ($o2["mimeType"] == "application/vnd.google-apps.folder") {
				return 1;
			} else {
				return strcmp($o1["name"], $o2["name"]);
			}
		});
		foreach ($json["files"] as $v) {
			fwrite($handle, "\n" . $v["id"] . "=\"" . $v["name"] . "\"");
		}
//		if (isset($json["nextPageToken"])) {
//			$query["pageToken"] = $json["nextPageToken"];
//		} else {
//			break;
//		}
	}
	fflush($handle);
	fclose($handle);
//}
$folders = parse_ini_file($file, true);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($_SESSION["token_expires_in"]) && isset($_SESSION["token_creation_time"])) { ?>
<?php if (($remain = ($_SESSION["token_expires_in"] + $_SESSION["token_creation_time"] - time())) > 0) { ?>
			<form action="a-drive-upload-file.php" method="post" enctype="multipart/form-data">
				<div>
					<select name="fileId">
						<option value="" selected="selected"></option>
<?php foreach ($folders["folder"] as $k => $v) { ?>
						<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
<?php } ?>
					</select>
					<a href="a-drive-renew-folders.php"><?php echo $messages["GOOGLE_DRIVE_RELOAD_FOLDER"]; ?></a>
				</div>
				<div>
					<input type="file" name="files[]" multiple="multiple"/>
				</div>
				<div>
					<input type="submit" name="submit" value="<?php echo TEMPLATE_TITLE; ?>"/>
					<input type="button" value="<?php echo $messages["GOOGLE_DRIVE_GOTO_FOLDER"]; ?>" onclick="window.open('https://drive.google.com/drive/u/0/folders/' + this.form.fileId.value, '_blank');"/>
				</div>
			</form>
<?php } else { ?>
			<div><?php echo sprintf($messages["GOOGLE_OAUTH2_TOKEN_EXPIRED"], -$remain); ?></div>
<?php } ?>
<?php } else { ?>
			<div><?php echo $messages["ERROR_NO_TOKEN"]; ?></div>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>