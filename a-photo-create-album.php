<?php

include_once("initial.php");

$data["album"]["title"] = $_POST["title"];
$data = json_encode($data);

include_once("HttpRequest.php");
$httpRequest = new HttpRequest("https://photoslibrary.googleapis.com/v1/albums");
$httpRequest->addHeader("Content-Type", "application/json");
$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
$httpRequest->addData($data);
$httpRequest->sendRequest("POST");

$json = json_decode($httpRequest->getResponseData(), true);
?>
<?php if (isset($json["error"])) { ?>
<?php
define("TEMPLATE_TITLE", $messages["GOOGLE_PHOTO_CREATE_ALBUM"]);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
			<div><?php echo sprintf($messages["ERROR"], $json["error"]["status"]); ?></div>
			<div><?php echo sprintf($messages["ERROR_MESSAGE"], $json["error"]["message"]); ?></div>
		</fieldset>
<?php include_once("t-html-end.php"); ?>
<?php } else { ?>
<?php
header("Location: p-photo-create-album.php");
?>
<?php } ?>