<?php

include_once("initial.php");
include_once("HttpRequest.php");

$httpRequest = new HttpRequest("https://www.googleapis.com/drive/v3/drives/" . $_POST["fileId"]);
$httpRequest->sendRequest("DELETE");
print_r($httpRequest);

die();
foreach (array_keys($_FILES["files"]["type"]) as $k) {
	$httpRequest = new HttpRequest("https://www.googleapis.com/upload/drive/v3/files");
	$httpRequest->addHeader("Content-Type", "application/octet-stream");
	$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
	$httpRequest->addHeader("X-Goog-Upload-Protocol", "raw");
	$httpRequest->addHeader("X-Goog-Upload-Content-Type", $_FILES["files"]["type"][$k]);
	$httpRequest->addFile($_FILES["files"]["tmp_name"][$k]);
	$httpRequest->sendRequest("POST");

	$data = array();
	$data["albumId"] = $_POST["albumId"];
	$data["albumPosition"]["position"] = "LAST_IN_ALBUM";
	$data["newMediaItems"][0]["simpleMediaItem"]["fileName"] = $_FILES["files"]["name"][$k];
	$data["newMediaItems"][0]["simpleMediaItem"]["uploadToken"] = $httpRequest->getResponseData();
	$data = json_encode($data);

	$httpRequest = new HttpRequest("https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate");
	$httpRequest->addHeader("Content-Type", "application/json");
	$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
	$httpRequest->addData($data);
	$httpRequest->sendRequest("POST");
}

header("Location: p-photo-upload-media.php");