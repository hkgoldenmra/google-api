<?php include_once("initial.php"); ?>
<!DOCTYPE html>
<html lang="zh-Hant-HK">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title><?php echo $messages["GOOGLE_SERVICE"]; ?></title>
		<link rel="stylesheet" type="text/css" href="css/default.css"/>
	</head>
	<body>
		<fieldset>
			<legend><?php echo $messages["GOOGLE_OAUTH2_SERVICE"]; ?></legend>
			<div><a href="p-oauth2-code.php"><?php echo $messages["GOOGLE_OAUTH2_GET_CODE"]; ?></a></div>
			<div><a href="p-oauth2-token.php"><?php echo $messages["GOOGLE_OAUTH2_GET_TOKEN"]; ?></a></div>
			<div><a href="p-oauth2-enquiry.php"><?php echo $messages["GOOGLE_OAUTH2_ENQUIRY_TOKEN"]; ?></a></div>
			<div><a href="a-oauth2-refresh.php"><?php echo $messages["GOOGLE_OAUTH2_REFRESH_TOKEN"]; ?></a></div>
			<div><a href="a-oauth2-revoke.php"><?php echo $messages["GOOGLE_OAUTH2_REVOKE_TOKEN"]; ?></a></div>
		</fieldset>
		<fieldset>
			<legend><?php echo $messages["GOOGLE_PHOTO_SERVICE"]; ?></legend>
			<div><a href="p-photo-create-album.php"><?php echo $messages["GOOGLE_PHOTO_CREATE_ALBUM"]; ?></a></div>
			<div><a href="p-photo-upload-media.php"><?php echo $messages["GOOGLE_PHOTO_UPLOAD_MEDIA"]; ?></a></div>
		</fieldset>
		<fieldset>
			<legend><?php echo $messages["GOOGLE_DRIVE_SERVICE"]; ?></legend>
			<div><a href="p-drive-upload-file.php"><?php echo $messages["GOOGLE_DRIVE_UPLOAD_FILE"]; ?></a></div>
			<div><a href="p-drive-delete-file.php"><?php echo $messages["GOOGLE_DRIVE_DELETE_FILE"]; ?></a></div>
			<div><a href="a-drive-clear-trash.php"><?php echo $messages["GOOGLE_DRIVE_CLEAR_TRASH"]; ?></a></div>
		</fieldset>
<!--
<?php var_dump($_SESSION); ?>
-->
	</body>
</html>