<?php

include_once("initial.php");
include_once("HttpRequest.php");

define("TEMPLATE_TITLE", $messages["GOOGLE_DRIVE_CLEAR_TRASH"]);
$httpRequest = new HttpRequest("https://www.googleapis.com/drive/v3/files/trash");
$httpRequest->addHeader("Authorization", $_SESSION["token_type"] . " " . $_SESSION["access_token"]);
$httpRequest->sendRequest("DELETE");
$json = json_decode($httpRequest->getResponseData(), true);
?>
<?php include_once("t-html-start.php"); ?>
		<fieldset>
			<legend><?php echo TEMPLATE_TITLE; ?></legend>
<?php if (isset($json["error"])) { ?>
			<div><?php echo sprintf($messages["ERROR"], $json["error"]["code"]); ?></div>
			<div><?php echo sprintf($messages["ERROR_MESSAGE"], $json["error"]["message"]); ?></div>
<?php } else { ?>
			<div><?php echo $messages["BASIC_SUCCESS"]; ?></div>
<?php } ?>
		</fieldset>
<?php include_once("t-html-end.php"); ?>